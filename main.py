import tkinter as tk
from tkinter import ttk
import csv


class StaffManagementApp:
    def __init__(self, root):
        self.root = root
        self.root.title("Облік робочого персоналу")

        self.staff_list = []

        # Створення таблиці
        self.table = ttk.Treeview(root, show="headings", columns=("Name", "Position", "Hours"))
        self.table.heading("Name", text="Ім'я")
        self.table.heading("Position", text="Посада")
        self.table.heading("Hours", text="Години та день")
        self.table.grid(row=0, column=0, columnspan=2)

        # Створення полів вводу
        self.name_label = ttk.Label(root, text="Ім'я:")
        self.name_label.grid(row=1, column=0)

        self.name_entry = ttk.Entry(root)
        self.name_entry.grid(row=1, column=1)

        self.position_label = ttk.Label(root, text="Посада:")
        self.position_label.grid(row=2, column=0)

        self.position_entry = ttk.Entry(root)
        self.position_entry.grid(row=2, column=1)

        self.hours_label = ttk.Label(root, text="Години та день:")
        self.hours_label.grid(row=3, column=0)

        self.hours_entry = ttk.Entry(root)
        self.hours_entry.grid(row=3, column=1)

        # Кнопки додавання та збереження
        self.add_button = ttk.Button(root, text="Додати", command=self.add_staff)
        self.add_button.grid(row=4, column=1)

        self.save_button = ttk.Button(root, text="Зберегти", command=self.save_to_file)
        self.save_button.grid(row=4, column=0)

    def add_staff(self):
        name = self.name_entry.get()
        position = self.position_entry.get()
        hours = self.hours_entry.get()

        self.staff_list.append((name, position, hours))

        self.table.insert("", tk.END, values=(name, position, hours))

        # Очищення полів вводу після додавання запису
        self.name_entry.delete(0, tk.END)
        self.position_entry.delete(0, tk.END)
        self.hours_entry.delete(0, tk.END)

    def save_to_file(self):
        filename = "staff.csv"

        with open(filename, mode='w', newline='') as file:
            writer = csv.writer(file)
            writer.writerow(["Ім'я", "Посада", "Години та день"])

            for staff in self.staff_list:
                writer.writerow(staff)

        print(f"Збережено у файл {filename}")


if __name__ == "__main__":
    root = tk.Tk()
    app = StaffManagementApp(root)
    root.mainloop()
